describe('App', function () {
  it('.should() - assert that <title> is correct', function () {
    cy.visit('http://pz.lp.edu.ua');
    cy.title().should('equal', "Кафедра програмного забезпечення | Національний університет 'Львівська політехніка'");
  });

  context('search', function () {
    beforeEach(function () {
      cy.visit('http://pz.lp.edu.ua');
    });

    it('should fill the search bar and find the result', function () {
      cy.get('#edit-search-block-form--2').type('Шкраб Роман');
      cy.get('#edit-actions').click();
      cy.get('.search-result .title a').first().should('have.text', 'Шкраб Роман Романович')
    });

    it('should fill the search bar and show the no result message', function () {
      cy.get('#edit-search-block-form--2').type('There is nothing to look here');
      cy.get('#edit-actions').click();
      cy.get('.clearfix h2').first().should('have.text', 'За вашим запитом нічого не знайдено')
    })
  });

  context('localization', function () {
    it('should switch the localization from default to en', function () {
      cy.visit('http://pz.lp.edu.ua');
      cy.get('a.language-link').children().first().click();
      cy.window().its('location.href').should('eql', 'http://pz.lp.edu.ua/en');
      cy.get('.logo .color').should('have.text', 'Software Department');
    });

    it('should fill the search bar and show the no result message', function () {
      cy.visit('http://pz.lp.edu.ua/en');
      cy.get('.uk.last').children().first().click();
      cy.window().its('location.href').should('eql', 'http://pz.lp.edu.ua/uk');
      cy.get('.logo .color').should('have.text', 'Кафедра програмного забезпечення');
    })
  });

  context('number of views', function () {
    it('should fill the search bar and show the no result message', function () {
      cy.visit('http://pz.lp.edu.ua');
      cy.get('.statistics_counter.first').children().invoke('text').then((text) => {
        const firstCounter = parseInt(text);
        cy.visit('http://pz.lp.edu.ua');
        cy.get('.statistics_counter.first').children().invoke('text').then((text => {
          const secondCounter = parseInt(text);
          expect(secondCounter).to.equal(firstCounter + 1)
        }))
      })
    })
  });

  context('feedback', function () {
    it('should not allow to send the empty form', function () {
      cy.visit('http://pz.lp.edu.ua/uk/node/249');
      cy.get('textarea').type('Hello world!');
      cy.get('textarea').clear();
      cy.get('form input[type=submit]').should('be.disabled');
    });

    it('should send the feedback form and reload page', function () {
      cy.server();
      cy.visit('http://pz.lp.edu.ua/uk/node/249');
      cy.get('textarea').type('Hello world!');
      cy.get('form').eq(1).click();
      cy.get('textarea').should('have.text', '')
    })
  });

  context('dissertation page', function () {
    it('should filter the table and leave only the dissertation defended under the guidance of проф. Федасюка Д.В.', function () {
      cy.visit('http://pz.lp.edu.ua/Dissertations-Defended-by-the-Department-Staff');
      cy.get('div.element:not(.isotope-hidden)').should('have.length', 25);
      cy.get('a[data-filter=".ff"]').click();
      cy.get('div.element:not(.isotope-hidden)').should('have.length', 7);
    });

    it('should open the PDF with dissertation', function () {
      cy.visit('http://pz.lp.edu.ua/Dissertations-Defended-by-the-Department-Staff');
      cy.get('a[href="http://pz.lp.edu.ua/docs/autorefs/Gavrysh.pdf"]').click();
      cy.window().its('location.href').should('eql', 'http://pz.lp.edu.ua/docs/autorefs/Gavrysh.pdf');
    });
  });
});